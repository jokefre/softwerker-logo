# softwerker-logo

Repository zur strukturierten Diskussion und Datensammlung mit dem Ziel gemeinsam das Logo der Softwerker zu entwickeln.

Namenskonvention: 
1. Initialien des Autoren im Dateinamen 
2. Aufsteigende Nummerierung (ohne Erweiterung <- bspw. 02a,02a1,02b ...)